from django.db import models

# Create your models here.
class bloggs(models.Model):
    name=models.CharField(max_length=200)
    email= models.CharField(max_length=200)
    blog_title= models.CharField(max_length=200)
    blog_abstract= models.CharField(max_length=200)
    blogcontent= models.TextField()
    status=models.CharField(max_length=10,default='notdeleted')

class vloggs(models.Model):
    blog = models.ForeignKey(bloggs, on_delete=models.CASCADE,default=None)
    name=models.CharField(max_length=200)
    email= models.CharField(max_length=200)
    blog_title= models.CharField(max_length=200)
    blog_abstract= models.CharField(max_length=200)
    blogcontent= models.TextField()
    status=models.CharField(max_length=10,default='notdeleted')


# class aloggs(models.Model):
#     blog = models.ForeignKey(bloggs, on_delete=models.CASCADE)
#     name=models.CharField(max_length=200)
#     email= models.CharField(max_length=200)
#     blog_title= models.CharField(max_length=200)
#     blog_abstract= models.CharField(max_length=200)
#     blogcontent= models.TextField()
#     status=models.CharField(max_length=10,default='notdeleted')
