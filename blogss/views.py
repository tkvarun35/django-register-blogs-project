from django.shortcuts import render
import json
from django.http.response import HttpResponse
# import django_filters
from django.shortcuts import render
# from django.http import HttpResponse
from django.db import connection
from blogss.models import bloggs,vloggs
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
@csrf_exempt
def newblog(req):
     if req.method=="POST":
        #   users=bloggs()
            data=json.loads(req.body)
        #   users.name=data['name']
        #   users.email=data['email']
        #   users.blog_title=data['blogtitle']
        #   users.blog_abstract=data['abstract']
        #   users.blogcontent=data['content']
        #   users.save()
            bloggs.objects.create(name=data['name'],
                                  email=data['email'], 
                                  blog_title=data['blogtitle'],
                                  blog_abstract=data['abstract'],
                                  blogcontent=data['content'])
     return JsonResponse(data,status=200,safe=False)


@csrf_exempt
def newvlog(request):
     if request.method=="POST":
            data=json.loads(request.body)

            vloggs.objects.create(name=data['name'],
                                  email=data['email'], 
                                  blog_title=data['blogtitle'],
                                  blog_abstract=data['abstract'],
                                  blogcontent=data['content'],
                                  blog_id=data['blog'])
     return JsonResponse(data,status=200,safe=False)



def blogs(request):
    if request.method=="GET":
      items=bloggs.objects.exclude(status='deleted').values('blog_title','id')
    # items=bloggs.objects.only("blog_title","id").values()
      new=list(items)
    return JsonResponse(new,status=200,safe=False)


@csrf_exempt
def new(request):
    if request.method=="POST":
      data=json.loads(request.body)
      identity=data['id']
      test=bloggs.objects.filter(id=identity).values()
      new1=list(test)
    return JsonResponse(new1,status=200,safe=False)


# @csrf_exempt
# def new(request):
#     if request.method=="GET":
#     #   data=json.loads(request.body)
#     #   identity=data['id']
#       test=bloggs.objects.filter(id='1').values()
#       new1=list(test)
#     return JsonResponse(new1,status=200,safe=False)


@csrf_exempt
def delete(request):
    if request.method=="POST":
      data=json.loads(request.body)
      identity=data['id']
      new=bloggs.objects.get(id=identity)
      new.status='deleted'
      new.save()
    return JsonResponse({"msg":"Deleted Succcessfully"},status=200,safe=False)

@csrf_exempt
def upnew(request):
    if request.method=="POST":
        # new=bloggs()
         data=json.loads(request.body)
         identity=data['id']
         new=bloggs.objects.filter(id=identity).values('blogcontent')
        # print(new)
         k=list(new)
    return JsonResponse(k,status=200,safe=False)



@csrf_exempt
def update(request):
    if request.method=="POST":
        # new=bloggs()
         data=json.loads(request.body)
         identity=data['id']
         new=bloggs.objects.get(id=identity)
        # print(new)
         new.blogcontent=data['newcontent']
         new.save()
    return JsonResponse({"msg":"Updated Succcessfully"},status=200,safe=False)



@csrf_exempt
def orderby(request):
    if request.method=="POST":
         #new=bloggs.objects.values().order_by('?')
         #new=bloggs.objects.values().order_by('vloggs__name','name').reverse()
         #new=bloggs.objects.values().order_by('name').reverse()
         #new=bloggs.objects.values().order_by('name').distinct()
         #new=bloggs.objects.filter(name__startswith='Avinash').values()
         #new=bloggs.objects.values_list('id',flat=True).reverse()
         #new=bloggs.objects.values_list('name',named=False).reverse()
         #new=bloggs.objects.none().values().reverse()
         #new=vloggs.objects.select_related('blog').order_by('blog_id').values()
         #new=vloggs.objects.values()
         #new=bloggs.objects.filter(status='notdeleted').count()
         new=bloggs.objects.values().latest('id')
         k=list(new)
    return JsonResponse(k,status=200,safe=False)