from django.urls import path

from . import views

urlpatterns = [
    path('newblog/', views.newblog, name=''),
    path('newvlog/', views.newvlog, name=''),
    path('blogs/', views.blogs, name=''),
    path('view/', views.new, name=''),
    path('delete/', views.delete, name=''),
    path('update/', views.update, name=''),
    path('upnew/', views.upnew, name=''),
    path('order/', views.orderby, name=''),
]