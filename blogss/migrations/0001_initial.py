# Generated by Django 2.0 on 2021-12-03 14:35

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='bloggs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('email', models.CharField(max_length=200)),
                ('blog_title', models.CharField(max_length=200)),
                ('blog_abstract', models.CharField(max_length=200)),
                ('blogcontent', models.TextField()),
                ('status', models.CharField(default='notdeleted', max_length=10)),
            ],
        ),
    ]
